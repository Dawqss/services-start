import { loggingService } from "./logging.service";
import {EventEmitter, Injectable} from "@angular/core";

@Injectable()
export class AccountsService {
    accounts = [
        {
            name: 'Master Account',
            status: 'active'
        },
        {
            name: 'Testaccount',
            status: 'inactive'
        },
        {
            name: 'Hidden Account',
            status: 'unknown'
        }
    ];

    constructor(private logService: loggingService) {

    }


    statusUpdated = new EventEmitter<string>();

    addAccount(name: string, status: string) {
        this.accounts.push({name: name, status: status});
        this.logService.logStatusChange(name);
    }

    updateStatus(id: number, status: string) {
        this.accounts[id].status = status;
        this.logService.logStatusChange(status);
    }
}