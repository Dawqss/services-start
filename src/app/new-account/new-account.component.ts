import { Component } from '@angular/core';
import { loggingService } from '../logging.service';
import {AccountsService} from "../accounts.service";

@Component({
  selector: 'app-new-account',
  templateUrl: './new-account.component.html',
  styleUrls: ['./new-account.component.css'],
  providers: []
})
export class NewAccountComponent {

  constructor(private logService: loggingService, private accService: AccountsService) {
    this.accService.statusUpdated.subscribe(
        (status: string) => alert('New Status: ' + status)
    );
  }

  onCreateAccount(accountName: string, accountStatus: string) {
      this.accService.addAccount(accountName, accountStatus);
  }
}
