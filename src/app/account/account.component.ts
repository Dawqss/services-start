import { Component, Input } from '@angular/core';
import {loggingService} from "../logging.service";
import {AccountsService} from "../accounts.service";

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css'],
  providers: []
})
export class AccountComponent {

  @Input() account: {name: string, status: string};

  @Input() id: number;

  constructor(private logService: loggingService, private accService: AccountsService) {}

  onSetTo(status: string) {
    this.accService.updateStatus(this.id, status);
    this.accService.statusUpdated.emit(status);
  }
}
